import click
import requests
import socket
import sys
from threading import Thread

class Th(Thread):
    def __init__ (self, host, port):
        Thread.__init__(self)
        self.host = host
        self.port = port
    
    def run(self):
        try:
            r = requests.get('http://' + self.host + ":" + self.port, stream=True)
            end_host = r.raw._fp.fp.raw._sock.getpeername()
            click.echo('Host: ' + end_host[0] + ":" + str(end_host[1]) + ' Response: ' + str(r.status_code))
        except Exception:
            sys.stderr.write('\nERROR: Check your arguments!\nThis cli do not support https, ips or unexistent hostnames.')
            sys.exit(1) 

def host_list(host):
    ip_list = []
    split = []
    if ":" in host:
        split = host.split(":")
    else:
        split.append(host)
        split.append('80')
    addrs = socket.getaddrinfo(split[0],int(split[1]),0,0,0)
    for result in addrs:
        if '::' not in result[-1][0]:
            ip_list.append(result[-1][0])
    ip_list = list(set(ip_list))
    return {'ip_list': ip_list, 'port': split[1]}

@click.command()
@click.option('--host', prompt='Hostname/IP to check',
              help='The hostname/ip to check.')
@click.option('--number', prompt='Number of time to check a given host',
              help='The number of times to check.')
def hcli(host, number):
    result = host_list(host)
    print(result)
    for h in result['ip_list']:
        for x in range(int(number)):
            if result['port'] == '443':
                print('Use non-ssl port for communication - this cli does not support https.')
                break
            else:
                a = Th(h, result['port'])
                a.start()

if __name__ == '__main__':
    try:
        hcli()
    except Exception:
        sys.stderr.write('ERROR: Check your arguments!\nThis cli do not support https, ips or unexistent hostnames.')
        sys.exit(1)
