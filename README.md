# Disclaimer

Requires python 3.x - Tested in python 3.7

This little cli takes a hostname as an argument. **IP addresses** and **https TLS-SSL** and **invalid hostnames** are not supported.

The lookup of the hostname will bring all ip addresses associated to it and a multi-threading check will be performed.

## Installation

pip install -r requirements.txt

## Running
Run: _python3 hcli.py_

Supply a valid _hostname_ and the _number_ of times to check.

